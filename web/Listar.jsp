<%-- 
    Document   : Listar
    Created on : 21/04/2016, 01:31:35 AM
    Author     : carmelo
--%>
<%@page import="javax.servlet.jsp.jstl.sql.ResultSupport"%>
<%@page import="javax.naming.spi.DirStateFactory.Result"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Beans.Empleados"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%

    ArrayList<Empleados> emps = (ArrayList<Empleados>) session.getAttribute("listaemp");
    ResultSet res = (ResultSet) session.getAttribute("tamaño");

//Devuelve el número de registros en la tabla.
    float nRegistros;
    if (res.next()) {
        nRegistros = res.getFloat(1);
    } else {
        nRegistros = 0;
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">WebSiteName</a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#">Empleados</a></li>
                        <li><a href="#">Productos</a></li> 

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" align="center">
            <table class="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Telefono</th>

                    </tr>
                </thead>
                <tbody>
                    <% for (Empleados m : emps) {%>
                    <tr>
                        <td><%= m.getId()%></td>
                        <td><%= m.getNombre()%></td>
                        <td><%= m.getTelefono()%></td>
                        <td>
                            <a class="btn btn-info" href="ShowServlet?mascota_id=<%=m.getId()%>">Detalles</a> 
                            <a class="btn btn-warning" href="EditarServlet?mascota_id=<%=m.getId()%>">Editar</a>
                        </td>
                    </tr>
                    <% }%>
                </tbody>
            </table>
        </div>
        <nav aria-label="Page navigation example" class="container">
            <ul class="pagination justify-content-center">
                <%
                    int pg = 0;
                    if (request.getParameter("pag") == null) {
                        pg = 0;
                    } else {
                        pg = Integer.parseInt(request.getParameter("pag"));

                    }
                %>
                <li class="page-item"><a class="page-link" href="Listar?pag=<%=Integer.parseInt(request.getParameter("pag")) - 1%>">Previous</a></li>
                    <%
                        System.out.println(nRegistros);
                        for (int j = 0; j < nRegistros / 5; j++) {
                    %>
                <li class="page-item"><a class="page-link" href="Listar?pag=<%=j%>"><%=j + 1%></a></li>
                    <%
                        }
                    %>
                <li class="page-item"><a class="page-link" href="Listar?pag=<%=Integer.parseInt(request.getParameter("pag")) + 1%>">Next</a></li>
            </ul>
        </nav>


        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function (){
            $(#tabla).DataTable();
            });
            https://code.jquery.com/jquery-1.12.4.js
        </script>

<<<<<<< HEAD
session.setAttribute("pagina",j);
                            %>
=======
>>>>>>> origin2/master

    </body>
</html>
