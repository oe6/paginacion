/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
 
public class ConectaDB{
 
 
//variables miembro
 
    private String db;
    private String host;
    private String user;
    private String pass;
    private String url;
    private Connection conn = null;
    private Statement estancia;
    private String driverClassName;

 
//CONSTRUCTORES
 
    //Constructor que toma los datos de conexion por medio de parametros
    public ConectaDB(String usuario, String clave, String url, String driverClassName) {
        this.db = db;
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.url = url;
        this.driverClassName = driverClassName;
    }
 
    //Constructor que crea la conexion sin parametros con unos definidos en la clase
    //(meter los datos correspondientes)
    public ConectaDB() {
        //poner los datos apropiados
        this.db = "EMPLEADOS";
        this.host = "jdbc:mysql://localhost:3306/";
        this.user = "root";
        this.pass = "";
        this.driverClassName = "com.mysql.jdbc.Driver";
        this.url= host + db + "?user=" + user + "&password="+pass;

    }

    //metodos para recuperar los datos de conexion
    public String getPass() {
        return pass;
    }
 
    public String getUrl() {
        return url;
    }
 
    public String getUser() {
        return user;
    }
 
    public Connection getConn() {
        return conn;
    }
 
    public String getDriverClassName() {
        return driverClassName;
    }
 
    //metodos para establecer los valores de conexion
    public void setpass(String pass) {
        this.pass = pass;
    }
 
    public void setUrl(String url) {
        this.url = url;
    }
 
    public void setUser(String user) throws SQLException {
        this.user = user;
    }
 
    public void setConn(Connection conn) {
        this.conn = conn;
    }
 
    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }
 
//la conexion propiamente dicha
 
    public Connection conectar() {
        try {
            Class.forName(this.driverClassName);
            this.conn = DriverManager.getConnection(this.url, this.user, this.pass );
 
        } catch (ClassNotFoundException | SQLException err) {
            System.out.println("Error " + err.getMessage());
        }
        return conn;
        
    }
    //Cerrar la conexion
 
    public void cierraConexion() throws SQLException {
        this.conn.close();
    }
 
//METODOS PARA TRABAJAR CON LA BASE DE DATOS
 
   
 
}