/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Beans.Empleados;
import java.io.IOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author carmelo
 */
@WebServlet(name = "Listar", urlPatterns = {"/Listar"})
public class Listar extends HttpServlet {

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            HttpSession session = request.getSession(true);
            int pagina=Integer.parseInt(request.getParameter("pag"));
            ConectaDB Conexion = new ConectaDB();
            java.sql.Connection cn = Conexion.conectar();
            Statement st = cn.createStatement();
            ResultSet rs;
            ResultSet rst;
            
            String sql="SELECT count(*) as Id FROM Empleados";
            String consulta = "Select * from Empleados LIMIT 5 OFFSET "+(pagina)*5+";";
            
            ArrayList<Empleados> arremp = new ArrayList<Empleados>();
            rs = st.executeQuery(consulta);

            while (rs.next()) {
                Empleados em = new Empleados();
                em.setId(rs.getInt(1));
                em.setNombre(rs.getString(2));
                em.setTelefono(rs.getString(3));
                 arremp.add(em);
            }
            
            rst=st.executeQuery(sql);
            
            
            
            session.setAttribute("listaemp", arremp);
           session.setAttribute("pag", pagina);
           session.setAttribute("tamaño", rst);
            request.getRequestDispatcher("Listar.jsp").forward(request,response);
            
            rs.close();
                    
            cn.close();

        } catch (SQLException ex) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
